#!/bin/bash

for s in lcl_scenario lcl_no_battery_scenario lcl_fixed_price1_scenario; do
  time python3 ./lcl/driver.py -s scenario.${s} -i 2000 -t 1e-4 -l 2e-3 -o ${s}-data
done
