# Contents

  + <a href="#1-introduction">1. Introduction</a>
  + <a href="#2-scope">2. Scope</a>
  + <a href="#3-experiment-setup">3. Experiment Setup</a>
  + <a href="#4-results">4. Results</a>
  + <a href="#5-source-of-flexibility-in-our-results">5. Source of Flexibility in Our Results</a>
  + <a href="#6-limitations-and-improvements">6. Limitations and Improvements</a>
  + <a href="#7-conclusion">7. Conclusion</a>
  + <a href="#8-notes-on-experiment-setup-reproduction">8. Notes on Experiment Setup & Reproduction</a>

# 1. Introduction
We attempted to replicate some of the experimental results published in a paper titled *[Optimal Demand Response Based on Utility Maximization in Power Networks](http://www.cds.caltech.edu/%7Echenlj/papers/ODemandResponse.pdf)* authored by Na Li, Lijun Chen and Steven H. Low in 2011.

In this preliminary report we present our results, and compare our results to those in the reference paper. Our results are largely consistent with those published in the paper, and serve to validate the fundamental result of the paper, which was to show the proposed method is effective for achieving an optimal electricity allocation in a deterministic day-ahead scenario, and also to show the general characteristics of the method.

Very similar overall trends and relative effects of the method are present in our results. However, our results do differ in some ways. A major limitation in our ability to reproduce the reference results precisely was that at the time of compiling the report, we did not have access to the exact parameters used in the reference paper. Specifically:

  1. We did not use exactly the same cost function.
  2. We did not use exactly the same parameters to the appliance utility curves.
  3. For one device types we used a similarly shaped, but ultimately different family of utility curve.

Even so, our results give us few reasons to doubt the published results could be replicated precisely with the right parameters.

In this report, we analyze where and why our results differ from those in the reference paper. Following this, we provide an in depth discussion about the effectiveness of the solution produced by the method. Finally, we discuss limitations of the method and how it might be improved.

# 2. Scope
The primary objectives of this preliminary report are to:

  1. Reproduce and validate selected results from the [Optimal Demand Response Based on Utility Maximization in Power Networks](http://www.cds.caltech.edu/%7Echenlj/papers/ODemandResponse.pdf) paper.
  2. Provide a more in depth analysis of certain aspects of the method. Specifically:
    1. The practicality of method.
    2. Where and how flexibility lead to savings in the solution.
  3. Comment on where it could be improved, and any other observations.

In conducting this experiment we implemented our own simulator tool that enabled us to simulate the mechanism and recreate the domain model described in the original  The code is available [here](https://gitlab.com/30523FSET/powermarket).

# 3. Experiment Setup
Li, Chen, Low conducted three experiments:

  1. A comparative analysis of the proposed optimization method with different pricing scenarios.
  2. An analysis of the effect of different battery cost parameters.
  3. Performance of the real-time pricing scheme with increasing number of users.

We only replicate the first experiment, which is the pertinent one in our opinion. In this experiment, Li, Chen, Low compared the price structure resulting from their optimization method - which we call the *LCL method* or just *LCL* here in - with 4 other price structures:

  + Base case.
  + Flat Price 1 (FP1).
  + Flat Price 2 (FP2).
  + LCL with no battery.

In the *base case* scenario, the user doesn't care about price (or equivalently the price is zero). In *FP1* and *FP2* a flat price is used across the entire planning window, and users respond to the flat price in accordance with their utility functions. Please refer to the original paper for details of how details of flat prices, *FP1*, and *FP2* were derived.

The experimental parameters are as close as practically possible to those outlined in the paper. However, the parameters deviate in a the following ways:

  * The utility function parameters we used are different. The authors only published the form utility functions not the exact parameters they used in most cases. Some parameters were listed for the battery, and we used these. The other parameters we fined tuned by trial and error over a couple of iterations so our numbers were in a ballpark range of the numbers published in the paper.
  * We used a concave polynomial of degree 4 for the utility function of lights and entertainment devices instead of the function listed in the paper. This and other utility functions we used are shown in <a href='#f1'>figures 1 to 4</a>.
  * The cost function we used is lower valued. The original paper used a piecewise defined quadratic with strictly increasing major coefficients. Upon request, we were able to obtain a copy of the original cost function from the authors. We had already coded our simulator to use a polynomial of arbitrary degree instead of a piecewise quadratic. We used least squares regression to fit a polynomial of degree 3 to the original piecewise quadratic. The fit *appeared* to be very precise, but in our simulation this results in cost which were in the order of half those published in the original paper. It's possible we misread the provided code or the cost function in the code had changed since publication. In any case, this is not a major concern to us as it doesn't influence us showing the viability of the method. It explains some difference in scale of some results however.

### Market Settings
Our algorithm used a constant step size. The process terminated when either a minimum stability in the consumption from one step to the next was reached, or a maximum number of steps was reached. The results in <a href='#t1'>table 1</a> list the termination condition that occurred in each case.

    stepsize: 0.002
    min stability (avg change in total consumption): 0.0001
    max steps: 2000
    cost function: 0.00045Q^3 + 0.0058Q^2 + 0.024Q where is total power consumption.

  -  **Note in hind sight the stepsize and min stability are very conservative**. A minimum stability of 0.0001 corresponds to and average change between step of 0.1W in aggregate. The <a href='#termination-conditions-convergence'>Termination Conditions & Convergence</a> section below also comments on step size and stability of our solutions.
  - **Note**. The reference paper dod not mention what termination conditions were used. In hind sight low excess demand is probably a more reasonable termination condition than change in demand. This is the definition of general equilibrium in market models.

### Network Structure & Device Settings
The devices and parameters are the same as described in reference paper, with the exception of the utility curve parameters (discussed in the next section). In brief:

  + Each house has five devices plus a battery:
    - Aircon.
    - Lighting.
    - Entertainment (TV-AV).
    - Washer.
    - E-Car (AKA PHEV).
  + Each device has certain constraints on it's power consumption. Specifically a min/max power rating, and possibly a cumulative consumption constraint. Example the E-Care must reach a minimum charge.
  + There are two types of household: stay at home and working. The major difference is working house holds all return home at 18:00 and start using power. They don't use any power before that.
  + The simulated day is divided into 24 1H time-slots. The day starts at 08:00.

*Please refer to the reference paper for further details, and a listing of the constraints that were placed on each device.*

### Utility Curves
<a href='#f1'>Figures 1 to 4</a> below show the different shapes of the utility curves we used across all five devices and the battery. Note the 'utility' curve for the battery is equivalent the negative of the cost of operation. The utility curve type for *entertainment* is the same as for *lighting*, and *washer* is the same type as the *e-car*. The utility curve for *aircon* is expressed in terms of temperature. This is mapped to power consumption via the thermal parameters of the aircon and house.

The utility curve we used for lighting and entertainment differs to the one used in the reference paper, although it has the same shape (strictly concave and increasing min to max). All the other curves are the same although the parameters - such as scaling factors - likely differ, as we did not have access to the exact parameters used at the time of compiling this report.

*In defense of using different utility curves and/or parameters;* note that utility curves are highly subjective in practice, and will differ in a practical implementation of the method, and between practical implementations, depending on application domain and design criteria. The fact that we used similar but not identical utility curves and achieved similar performance only goes to show a level of robustness in the results.

<table>
  <tr>
    <td width="50%">
        <figure>
            <img width='98%' name='f1' src='img/lighting-utility-curve.png'></img><br/>
            <small>Figure 1: Example utility curve for Lighting and Entertainment.</small>
        </figure>
    </td>
    <td width="50%">
        <figure>
            <img width='98%' name='f2' src='img/ecar-utility-curve.png'></img><br/>
            <small>Figure 2: Utility curve for E-car and Washer.</small>
        </figure>
    </td>
  </tr>
  <tr>
    <td width="50%">
        <figure>
            <img width='98%' name='f4' src='img/aircon-utility-curve.png'></img><br/>
            <small>Figure 4: Utility curve for aircon. The two vertical lines show the min/max comfortable temperatures. The x-axis has units of degrees (although scaled to be from 0 to 1 in this example).</small>
        </figure>
    </td>
    <td width="50%">
        <figure>
            <img width='98%' name='f3' src='img/battery-utility-curve.png'></img><br/>
            <small>Figure 3: Utility curve for battery. In the case of the battery 'utility' is the cost of operating the battery.</small>
        </figure>
    </td>
  </tr>
</table>

# 4. Results

### Overall Effectiveness of the LCL Method
The figures below show the overall effect of the LCL optimization in comparison with the base case. These figures are similar to the ones in the reference paper, but generated with our data. They show the same overall patterns, with the major effect of the optimization method being a flattening of the load profile to maximize the welfare objective.

<table>
  <tr>
    <td width='32%'>
        <figure>
            <img src='lcl_scenario-data-report/demand-zero-price-agent-home1.png'></img><br/>
            <small>Figure 5: Home 1 (stay at home type) consumption profile when the user doesn't care about price.</small>
        </figure>
    </td>
    <td width='32%'>
        <figure>
            <img src='lcl_scenario-data-report/demand-zero-price-agent-home5.png'></img><br/>
            <small>Figure 6: Home 5 (working type) consumption profile when the user doesn't care about price.</small>
        </figure>
    </td>
    <td width='32%'>
        <figure>
            <img src='lcl_scenario-data-report/total-demand.png'></img><br/>
            <small>Figure 7: Total demand comparision, base case vs optimal.</small>
        </figure>
    </td>
  </tr>
  <tr>
    <td width='32%'>
        <figure>
            <img src='lcl_scenario-data-report/demand-final-agent-home1.png'></img><br/>
            <small>Figure 8: Home 1 (stay at home type) optimal consumption profile.</small>
        </figure>
    </td>
    <td width='32%'>
        <figure>
            <img src='lcl_scenario-data-report/demand-final-agent-home5.png'></img><br/>
            <small>Figure 9: Home 5 (working type) optimal consumption profile.</small>
        </figure>
    </td width='32%'>
    <td width='32%'>&nbsp;</td>
  </tr>
</table>

### Termination Conditions & Convergence
<a href='#f10'>Figure 10</a> shows, that for *FP1*, and *LCL* with and without a battery, the simulation did not reach our required level of stability for termination, instead terminating when 2000 steps was reached (The other two scenarios require no iterations). <a href='#f10'>Figure 10</a> shows that approximately 90% gains in welfare and satisfaction were achieved in the first 500 steps, and indicates the final solution is very close to optimal. We assume that the difference between the true optimal and our results is negligible here in unless otherwise stated.

*In hind sight, our stepsize turned out to be very conservative for the given scenario*. However, the steps has a little effect on the end results, only really effecting the time it takes longer to get there. The section on <a href='#step-size'>Step Size</a> a the end of this report discusses step size selection.



<figure>
    <img name='f10' width='380px' src='lcl_scenario-data-report/welfares.png'></img><br/>
    <small>Figure 10: How welfare and total satisfaction changed with each step of the market.</small>
</figure>

### Comparison with other Pricing Scenarios
The results of the comparative analysis in the reference paper are reproduced in <a href='#t2'>table 1</a>. Our corresponding results are shown in <a href='#t1'>table 2</a>. Figures 11 to 18 show rows of tables 1 and 2 graphically to emphasize trends in the results. The results are discussed in the following sections.


|Statistic|Base Case|FP1|FP2|LCL no battery|LCL|
|-|-|-|-|-|-|
|Load Factor|0.3587|0.4495|0.4904<small>(a)</small>|0.7146|0.8496|
|Peak Demand|18.8|14.7|13|8.76|7.29|
|Total Demand|162|158|153|150|148|
|Total Cost|64.41|45.49|41.8|32.82|31.5|
|Total Payment|137.4|54.59|58.56|57.42|55.69|
|Total User Utility|212.41|201.72|200.14|198.82<small>(b)</small>|198.82<small>(b)</small>|
|Total User Satisfaction <small>(c)</small>|75.01|147.14|141.57|141.4|143.13|
|Social Welfare <small>(d)</small>|148|156.24|158.33|166|167.32|

<small size='small'>
<a name='t1'></a><small>Table 1: Comparative Statistics - Reference Results</small>

<small>a: The published load factor number did not match the published peak demand and total demand numbers. We recalculated the load factor, based on the published peak demand and total demand. The recalculated number is the number shown, and is closer to what we expected. The original value is 0.4577.</small>

<small>b: It seems unlikely to us the the total utility is the exactly the same while the total demand is different.</small>

<small>c: TotalUserSatisfaction = TotalUserUtility - TotalPayment</small>

<small>d: SocialWelfare = TotalUserUtility - TotalCost</small>
</small>

---

|Statistic|Base Case|FP1|FP2|LCL no battery|LCL|
|-|-|-|-|-|-|
|Load Factor|0.4043|0.4218|0.5642|0.6278|0.7764|
|Peak Demand|21.2318|18.0325|13.1225|11.5624|9.4532|
|Total Demand|206.0358|182.5397|177.6874|174.2145|176.139|
|Total Cost|37.7844|26.4414|18.7884|17.0498|16.4157|
|Total Payment|88.6783|26.5414|33.4309|34.9399|33.1395|
|Total User Utility|203.5094|202.1508|201.356|200.3802|200.6507|
|Total User Satisfaction|114.8311|175.6094|167.9251|165.4403|167.5111|
|Social Welfare|165.725|175.7094|182.5675|183.3305|184.2349|
|Steps|N/A|2000|N/A|2000|2000|
|Stability (avg in consumption)|N/A|0.000017|N/A|0.000145|0.000184|

<a name='t2'></a><small>Table 2: Comparitive Statistics - Our Results</small>

---

<figure>
<table>
  <tr>
    <td><img width='380px' src='img/load-factor-comparison.png'></img></td>
    <td><img width='380px' src='img/peak-demand-comparison.png'></img></td>
    <td><img width='380px' src='img/total-demand-comparison.png'></img></td>
  </tr>
  <tr>
    <td><img width='380px' src='img/total-utility-comparison.png'></img></td>
    <td><img width='380px' src='img/total-cost-comparison.png'></img></td>
    <td><img width='380px' src='img/total-payment-comparison.png'></img></td>
  </tr>
  <tr>
    <td><img width='380px' src='img/total-satisfaction-comparison.png'></img></td>
    <td><img width='380px' src='img/welfare-comparison.png'></img></td>
    <td>&nbsp;</td>
  </tr>
</table>
<small>Figures 11 to 18: Graphically compares trends in rows of tables 1 and 2.</small>
</figure>

---

#### Demand - Total, Peak, and Load Factor
Our overall consumption is greater in across all price scenarios, as is the peak demand. For the base case scenario, the user should spare no expense in achieving maximum utility. This should mean the price and the utility function play no part in the difference in total demand. In theory, the only device that could account for this difference is the air conditioners. It takes more or less power to achieve the preferred temperature and the air conditioner preferred temperature was randomly generated. It does not seem likely that the air-conditioners were solely responsible for this difference. More likely there is a discrepancy in the published and actual power consumption bounds.

Interestingly, in contrast to the reference results, in our results the total demand is actually higher in the LCL scenario with battery than without it. This should not be unexpected; The net flow of electricity to and from the battery is zero. But the battery essentially allows users to take power at time when it is cheap and draw from the battery when power is expensive. Thus, instead of curtailing load when the price is high, load can be drawn from the battery. It's unclear why the reference results do not also show the same.

The trend in load factors across price scenarios from left to right is very similar in both result sets as shown in <figure 4>. We did not achieve as high a load factor for the either of the LCL scenarios. The two most likely causes for this divergence are:

  1. One or more air conditioners in our simulation being less flexible at peak times due to the randomized settings.
  2. The different shape of the utility curves.

The effect of the utility curve is further discussed in the <a href='#Utility'>Utility</a> section below.

A notable difference is that in our simulation the load factor was considerably more for the *FP2* scenario than *FP1* as shown in <a href='#f11'>figure 11</a>. This is at least partly due to the difference in fixed prices, which are different because the cost function used is different. <a href='#t3'>Table 3</a> shows that in relative terms, the relative price difference between FP1 and FP2 in our results is more than twice that in the reference paper.

|Statistic|Ours  |Theirs|
|-|-|-|
|FP1 Price|0.1454|0.3455|
|FP2 Price|0.1881|0.3827|
|Relative Change|0.2272|0.0973|

<a name='t3'></a><small>Table 3: Relative change in price between FP1 and FP2 for our experiment and reference results</small>

---

### Costs, Satisfaction and Welfare
Our cost function is clearly lower valued than in the reference results. Note however:

  + At the very least, our cost function it is a strictly convex function which is all that is required by the method.
  + The shape of our cost curve is based on a piecewise quadratic with strictly increasing major terms, just as in the reference paper.
  + An absolute constant positive offset added to the cost function should have no effect on the relative difference between the base case solution and the optimal solution. Only the rate of change in cost has an effect on the solution.
  + The relative change costs between the baseline solution and optimal solution are much more closely aligned, being 2.30 in our implementation and 2.04 in the published reference.

For these reasons, we conclude that this difference in costs, while not ideal, does not invalidate our results as applicable to our objectives.

`Total Payment`, `Total User Satisfaction`, and `Welfare` are all derived from the cost function, and also from `Total User Utility` in the latter two cases. The bulk of the  differences between the reference results and our results in these three statistics is due to the difference in the cost function. The more minor difference in the utility is discussed in the next section.

One interesting result is that `FP1` actually has the highest `Total User Satisfaction`. It's obvious from the definition why; the price paid by users is lowest under `FP1` and not outweighed by differences in utility. The reason the price is lower is that the LCL method uses the derivative of the cost function as the price which guarantees the total amount paid is higher than total costs with the given cost function: The lowest feasible price that covers costs is `p = C(Q)/Q`. In the LCL method price is `p = C'(Q)`. Given `C` is a strictly convex function, the LCL price is guaranteed to be larger than `C(Q)/Q` for all `Q > 0` when `C(Q) = 0`. *But note that the LCL method does not aim to find the lowest price for users that covers cost*. It finds the optimal social welfare, which maximizes utility and minimizes cost in aggregate. A price of `C'(Q)` is just *one* way to charge users. Whether this is actually a good way, are other ways user could be charged is considered [below](#how-to-charge-users).

### Utility
<a href='#f14'>Figure 14</a> shows that in our results the total change in utility between the base case and the other cases is much less that in the reference results. Amongst the other 4 scenarios the relative changes in utilities are much more similar. In general two factors contribute to the differences in total utility:

  1. The higher the rate of change of the cost function the greater the downward force on utility.
  2. The shape the utility curve can also have an effect. If the (concave) utility curve changes more rapidly, there will be less overall change in utility observed.

# 5. Source of Flexibility in Our Results
The reference report does not provide any significant analysis of how the power changes at a home or appliance level. In this section we drill down into home and device level statistics.

<a href='#t4'>Table 4</a> shows the total payment, consumption, utility and satisfaction, and average price for each individual home in the optimal solution. <a href='#t5'>Table 5</a> shows the same for the base case and <a href='#t6'>table 6</a> show the difference between the two.

Recall homes 5-8 are the workers who get home at 18:00 and don't consume any electricity before this time. This leads homes 5-8 consume about half as much as homes 1-4. In the base case, the average price for homes 5-8 is more than homes 1-4 because they are using more power at a time when there is more contention for power which drives up the total consumption and thus price. In contrast, there much less variance in the average price in the optimal scenario due the flattening of the demand profile. As we should expect, the average price for each user is also much lower in the optimal scenario due to this flattening and some reduction that occurs due to compromises of utility for savings.

### Device Flexibility
<a href='#t7'>Table 7</a> breaks down the change utility across homes by the device responsible. <a href='#t8'>Table 8</a> shows the corresponding reduction in consumption by the devices.

Table 7 and 8 show that the *washer* and *e-car* do not compromise utility or consumption at all. However,the load profiles of these devices are clearly different from the base case as shown in <a href='#f5'>figure 5 through 9</a> above. The reason total consumption and utility don't change is down to the shape of the utility curve of these two devices (<a href='#f2'>figure 2</a>). These devices have don't preference one time over any other (within their feasible operating times) so consumption is shifted to the lowest price time by the optimization method. There is also no reduction in consumption because the price never exceeds the linear slope of the utility curve of these devices. If it did, and the consumption could not be shifted to a cheaper time, the consumption would go to zero in the optimal scenario.

For the *aircon*, *lighting*, and *entertainment*, comparing tables 7 and 8 shows a relatively small change in outright utility resulted in a large reduction in consumption. <a href='#f19'>Figure 19</a> indicates how this came about for the lighting. It shows the utility curve for the lighting of home 1, with a horizontal line at the utility value in the optimal scenario. The point where the two lines intersect is determined only by the price and the gradient of the utility function. At this point the price equals the gradient of the lighting's utility curve. One can see that the power consumption was able to decrease a lot before it had any significant effect on utility. Then, quickly, the utility starts to change.

Table 8 shows that home 1 aircon made a reduction of 4.9476KW in the optimization result. This is because home 1 had the lowest preferred temperature of 73 degrees but also the highest acceptable temperature range of 3 degrees. This change in temperatures among devices are shown in <a href='#f20'> figures 20 and 21</a>.


|Statistic|Home1|Home2|Home3|Home4|Home5|Home6|Home7|Home8|Total|
|-|-|-|-|-|-|-|-|-|-|
|Total Payment|5.135|5.274|5.7|5.656|3.086|2.475|2.906|2.907|33.139|
|Total Consumption|27.814|28.373|30.524|30.31|15.949|13.158|14.912|15.099|176.139|
|Total Utility|14.469|21.232|68.857|60.451|7.699|5.086|10.424|12.432|200.65|
|Total Satisfaction|9.334|15.958|63.157|54.794|4.613|2.611|7.518|9.525|167.51|
|Avg Price|0.185|0.186|0.187|0.187|0.193|0.188|0.195|0.193||

<a name='t4'></a><small>Table 4: Home by Home LCL Results</small>

---

|Statistic|Home1|Home2|Home3|Home4|Home5|Home6|Home7|Home8|Total|
|-|-|-|-|-|-|-|-|-|-|
|Total Payment|13.27|12.736|12.583|12.68|10.093|8.865|9.481|8.97|88.678|
|Total Consumption|35.223|33.554|33.392|33.504|19.112|16.797|17.352|17.102|206.036|
|Total Utility|15.158|21.723|69.148|60.751|7.997|5.445|10.649|12.638|203.509|
|Total Satisfaction|1.888|8.987|56.565|48.071|-2.096|-3.42|1.168|3.668|114.831|
|Avg Price|0.377|0.380|0.377|0.378|0.528|0.528|0.546|0.525||

<a name='t5'></a><small>Table 5:Home Base Case Results</small>

---

|Statistic|Home1|Home2|Home3|Home4|Home5|Home6|Home7|Home8|Total|
|-|-|-|-|-|-|-|-|-|-|
|Total Payment|8.135|7.462|6.883|7.024|7.007|6.39|6.575|6.063|55.539|
|Total Consumption|7.409|5.181|2.868|3.194|3.163|3.639|2.44|2.003|29.897|
|Total Utility|0.689|0.491|0.291|0.3|0.298|0.359|0.225|0.206|2.859|
|Total Satisfaction|-7.446|-6.971|-6.592|-6.723|-6.709|-6.031|-6.35|-5.857|-52.679|
|Total Price|0.192|0.194|0.190|0.192|0.335|0.340|0.352|0.332||

<a name='t6'></a><small>Table 6: Home Difference Base Case take LCL</small>

---

|Device|Home1|Home2|Home3|Home4|Home5|Home6|Home7|Home8|Total|
|-|-|-|-|-|-|-|-|-|-|
|Aircon|0.4603|0.2625|0.063|0.0719|0.1099|0.1718|0.0375|0.0187|1.1955|
|E-car|0.|0.|0.|0.|0.|0.|0.|0.|0.|
|Washer|0.|0.|0.|0.|0.|0.|0.|0.|0.|
|Lighting|0.0916|0.0916|0.0912|0.0916|0.0916|0.0916|0.0915|0.091|0.7315|
|Entertainment|0.0956|0.0956|0.0956|0.0956|0.0589|0.0589|0.0589|0.0589|0.6180|
|Battery|0.0409|0.0409|0.0409|0.0409|0.0375|0.0375|0.0375|0.0375|0.3138|

<a name='t7'></a><small>Table 7: Home by Device Change in Utility Base Case to LCL</small>

---

|Device|Home1|Home2|Home3|Home4|Home5|Home6|Home7|Home8|Total|
|-|-|-|-|-|-|-|-|-|-|
|Aircon|4.9476|2.7141|0.676|0.7438|1.0332|1.5273|0.349|0.2122|12.2003|
|E-car|0.|0.|0.|0.|0.|0.|0.|0.|0.|
|Washer|0.|0.|0.|0.|0.|0.|0.|0.|0.|
|Lighting|1.446|1.4516|1.176|1.4344|1.4592|1.4407|1.419|1.1193|10.9462|
|Entertainment|1.0158|1.0158|1.0158|1.0158|0.6711|0.6711|0.6711|0.6711|6.7475|
|Battery|0.|0.|0.|0.|0.|0.|0.|0.|0.|

<a name='t8'></a><small>Table 8: Home by Device Change in Consumption Base Case to LCL</small>

---

<table>
  <tr>
    <td>
        <figure>
            <a name='f19'><img width='380px' src='img/lighting-utility-curve-home1.png' /></a>
            <small>Figure 19. Lighting utility curve for home 1 with utility at optimal solution shown.</small>
        </figure>
    </td>
    <td>
        <figure>
            <a name='f20'><img width='380px' src='lcl_scenario-data-report/aircons-init.png'></a>
            <small>Figure 20. Temperature in each home over day in base case. Home spare no expense bringing temperature to optimal.</small>
        </figure>
    </td>
    <td>
        <figure>
            <a name='f21'><img width='380px' src='lcl_scenario-data-report/aircons-final.png'></a>
            <small>Figure 21. Temperature in each home over day in optimal solution. Home compromise.</small>
        </figure>
    </td>
  </tr>
</table>

# 6. Limitations and Possible Improvements
This section discusses some limitations to the market mechanism proposed by Li, Chen, Low and how it could be improved.

### Step size
Li, Chen, Low did not comment on what step size selection they used in their market simulation, only saying the step size should be small enough to ensure convergence (to within some reasonably small threshold). We used a constant step size. The step size and minimum stability termination condition settings we used in our experiment of `0.002` and `0.0001` respectively, were *very* conservative. We repeated the experiment with stepsize increased by a factor of 10, 50, 100, 250 times. We also increased the minimum stability for termination by a factor of 10 to `0.001`. At 250 times the method is completely unstable and will not terminate. The table below show the results for the other settings.

Interestingly, with increasing step size from 10 to 100 times the algorithm converges to slightly better solutions, and in far less steps. Note however, that step size and termination condition are interdependent. When we tested with step size of `0.2` and the original stability condition of `0.0001` the algorithm failed to converge within that stability. With the lower stepsize of `0.02`, if the stability condition were smaller it could *eventually* converge to the same result.

| |Step size = 0.02|Step size = 0.1|Step size = 0.2|
|-|-|-|-|
|Load Factor|0.7825|0.7938|0.7942|
|Peak Demand|9.3864|9.2605|9.2523|
|Total Demand|176.2696|176.4324|176.3669|
|Total Cost|16.3722|16.3083|16.2899|
|Total Payment|33.0168|32.8392|32.7954|
|Total User Utility|200.6798|200.7486|200.7587|
|Total User Satisfaction|167.663|167.9094|167.9633|
|Social Welfare|184.3076|184.4403|184.4688|
|Steps|253|129|103|
|Stability (avg in consumption)|0.000995|0.000984|0.001|

<table>
  <tr>
    <td>
        <figure>
            <img name='f22' width='380px' src='lcl_scenario-data-large-steps-0.02-report/welfares.png'></img>
            <small>Figure 22. stepsize = 0.02</small>
        </figure>
    </td>
    <td>
        <figure>
            <img name='f23' width='380px' src='lcl_scenario-data-large-steps-0.1-report/welfares.png'></img>
            <small>Figure 23. stepsize = 0.1</small>
        </figure>
    </td>
    <td>
        <figure>
            <img name='f24' width='380px' src='lcl_scenario-data-large-steps-0.2-report/welfares.png'></img>
            <small>Figure 24. stepsize = 0.2</small>
        </figure>
    </td>
  </tr>
</table>

#### Step Size Selection Methodology
The above results show that the method can converge to a good solution in only a few (~less than 10) iterations with the right stepsize. The "sweet spot" in this particular scenario is around 1 to 5 Watts average change in consumption for the termination condition and a step size of 0.1 to 0.2. The issue is, our methodology for discovering this above was trial and error. In practice an automated method will be required.

**Update:** Subsequent to this report, we added a simple *limited minimization* adaption to the gradient ascent step algorithm. This allows us to use a large (but fixed) initial stepsize (we successfully tested with a stepsize of *1*) without risking non-convergence. There are many other adaptive step size approaches that could offer potentially better rates of convergence or faster processing.

### How to Charge Users?
In the <a href='#comparison-with-other-pricing-scenarios'>analysis above</a> we noted that the user actually pays less in the `FP1` pricing scenario than in the LCL optimization scenario. This leads to better out right user satisfaction. The reason for this is that the pricing arrangement Li, Chen, Low used in their optimization method leads to a large "profit" where as `FP1` does not.

FP1 price is calculated as the sum of costs over all time slots, divided by the sum of the consumption over all time slots. This price is calculated iteratively, using the same tatonnement process as usual, until stability.

Under a fixed setting, there is actually no need for the market at all. Each user can independently optimize according to the fixed price. In a real life setting, user would likely start from some independent optimization of their power usage and then collaborate indirectly via a market mechanism to find an even better, mutually beneficial solution. If the initial price were FP1, users would have no incentive to collaborate using the proposed market mechanism under it's current pricing scheme. Under the optimized result since the cost *are* lower we should be able to derive a price that is mutually beneficial and better for user than `FP1`. The question is what is that price?

One obvious alternative price is `C(Q,t)/Q(t)` ... With this price there is no "profit" at all. It is slightly different to `LP1` in that `LP1` averaged across all times `t` to give a single rate. The LCL tatonnement process should be stable with `C(Q,t)/Q(t)` too. We tested with this price, and as expected satisfaction is higher but social welfare lower, as for LP1 (note simulation ran with same data but after adaptive step size addition to code):

|Statistic|TOU Price 2 (with battery)|
|-|-|
|Load Factor|0.6897|
|Peak Demand|11.2384|
|Total Demand|186.0303|
|Total Cost|18.7544|
|Total Payment|18.7544|
|Total User Utility|202.3749|
|Total User Satisfaction|183.6205|
|Social Welfare|183.6205|
|Steps|29|
|Stability (avg in consumption)|0|

### Wealth Effects and Fairness
The market is inherently fair if agents have roughly equal wealth. However wealthy participants value dollars less than less wealthy participants relative to "utils". This makes wealthy individuals less willing to compromise. On the other hand, less wealthy participants get to trade utils for savings which they value more. Never the less, the outcomes will (todo prove this) be better for participants if there is no differentials in wealth.

Since wealth is an externality, and energy has to be paid for in money, it's hard to see how to solve this problem fairly, or whether we should bother at all.

### Peculiarities of The Distributed Algorithm
A pseudo-code for the distributed algorithm presented in the reference paper and reimplemented here is:

    demand = consumers.collect-initial-demand-forecasts()
    do:
      price = supply-side.get-price(demand)
      demand = consumers.update-demand(price)
    until there is no change in demand

According to the reference paper, there is a *single* supply side with a strictly convex cost curve (like a thermal generator). `get-price()` above, sets a price as the marginal cost of the single supply (the paper assumes the central agent running the above algorithm has access to the cost curve and can calculate the marginal cost directly from it). The `update-demand()` method broadcasts the current price to all consumers. The consumers make a step towards their optimal demand at the price away from their current demand, and then return this value. The `update-demand()` function then (synchronously) aggregates the consumer demands and returns that value.

There (at least) the following issues with this arrangement:

  1. It is assumed there is only a single supplier. How to incorporate many suppliers is not discussed in the paper.
  2. It is assumed the market is strictly partitioned into a demand side and a supply side. This precludes shared batteries and prosumers.
  3. Requiring the gradient descent to occur on the client side seems open to manipulation, or not robust, or both. Note, if agents *do* jump directly to optimal demand the algorithm will in most circumstance not reach an equilibrium.
    - A strategic agent might be able to "wreck" the process in the hope of exploiting other bugs in the system to get a better individual outcome. For example, in a real life deployment of this algorithm, if stability cannot be reached, contingency prices will have to settled on. By wrecking the equilibrium, a strategic agent can force this contingency to occur which might result in more favorable prices to themselves.
    - It seems conceptually simpler and clearer, for clients to simply set their optimal demand when given a price. This may not seem like a big deal (and maybe it is not) but when it comes to inter-operation of distributed devices made by different manufacturers, the simpler the communication protocols and semantics the better.
    - All things being equal, relying less on complexity in the distributed agents should lead to a more robust system.

The first issue *seems* relatively easy to solve. In the original version the central agent has to solve for margin cost. I.e. if *C(Q)* is the cost curve *p = C\`(Q)*. If multiple suppliers are present, the central agent first collects up all cost curves. Then, **supposing** suppliers have no upper or lower bound on their cost curves, it must solve a slightly more complicated *constrained* optimization problem to find *p*. Let *C\_i(Q\_i)* be the *i*-th cost curve. The central agent finds *p* such that *p = C\_i\`(Q\_i) = C\_j\`(Q\_j) all i,j* while *sum\_i Q\_i = Q*. If suppliers do have upper and/or lower bound on their cost curves (it is pretty reasonable tp assume they would), then the optimization problem is another step harder but still convex and sovlable. But now the supply side is now starting to resemble the demand side, and using a centralized calculation for the supply side goes against our original motivation for a decentralised algorithm in the first place. If there are only a few sources of generation, it is probably a reasonable compromise.

The second issue seems less easy to address. And the third issue is an inherent part of the algorithm so can't be addressed without changing the algorithm completely.

#### Price Adjustment Distributed Algorithms vs LCL
[Sandholm][Sandholm] partitions the family of market mechanisms to which the LCL mechanism belongs (dynamic, clearing, multicommodity auctions), into price orientated mechanisms and resource orientated mechanisms. Regardless of type, successful termination ("clearing") means all agents within the market *simultaneously* agree on both price and resource allocations. Up until that point they are in disagreement about either the price or the resources allocated (or both). As far as it goes, according to Sandholm the LCL mechanism is conceptually resource orientated. The other type of mechanism, price orientated, are perhaps more conventional.

Various price based approaches are possible. They vary mainly in the structure of the market bids and offers. Regardless, a suitable price orientated approach, should be able to find an equilibrium solution in all the cases that the LCL mechanism is able to. More over, using a price based approach seems to address *all three* of the issues listed above! It begs the questions then:

  1. Why not just use a price based mechanism?
  2. Is there *any* advantage to the LCL mechanism?
  3. If there is an advantage, it is likely that it is in rate of convergence. If this is so, then is it possible to overcome the 2nd issue listed above while maintaining LCL's distributed gradient ascent?

### Other Limitations
The following list *very* briefly flags some of the other limitations or potential area for improvement in the market mechanism, or just things we have questions about in general. This section is intended to be informal and serves as a non exhaustive list of bookmarks for future investigation.

  + **Real Time Coordination:** How to re-plan in the case of some change in requirements is not obvious and should be addressed.
  + The system is setup so the battery is isolated to each home. Users can't export charge from their battery to a different user. The same constraint would apply to solar panels or generators supposing the system could be extended to support these devices.
  + **Strategic Agents:** Strategy proofness, or at least robustness is not addressed in the reference paper. Suppose one agent refuses to compromise any utility but all other agents do. What will the final position of this uncooperative agent will be??
  + The stability of the solution relies on agent's defining separable and concave utility functions. The "central authority" or market operator has no visibility into that, and must trust the agents.


#### General Reliability & robustness consideration for a distributed market mechanism
In general, for a market based method to be practically useful we think at least the following criteria should be met:

      1. The market must move monotonically to a better solution at least initially.
      2. In the first step, and subsequent few steps (the market could be asynchronous. In that case a 'step' by average bid/price interactions per agent) of the market the gains should be large. The gains from only a small number of initial 'steps', while not optimal, should be worthwhile and useable as a contingency.
      3. The market should be able to handle one or more agents ceasing to participate after only a few steps or not participating at all.
    The adaptive stepsize effectively overcomes the first two issues. An issue with the 3rd is, detecting faulty agents. Also the market takes synchronous steps; at each iteration the market must wait for a reply from all agents.

# 7. Conclusion

 + We've verified the system proposed by Li, Chen, Low works and with similar performance reported in the original paper.
 + We noted some ways in which our results diverged from Li, Chen Low's results that we believe were not simply the result of using different experimental parameters.
 + We identified that an automated and/or adaptive step size is necessary if the system is to be practically useful.
 + We identified that selecting a tariff structure is an issue that was not discussed in depth in the original paper. The pricing scheme chosen by Li, Chen, Low may not fit the objectives in all cases because it tends to overcharge users.

# 8. Notes on Experiment Setup & Reproduction
An instance of the randomized agent configurations used in the simulation is stored [here](lcl_scenario_agents_set.pickle). You'll need to edit the lcl_scenario.py file to load this pickled configuration rather than regenerating new random configuration to regenerate results. Reports can be generated off the data dump of a simulation. The data dumps for this report is [here](./data).

**LCL with Battery & Base Case**

The command line output of the tool shows all of the statistics in <a href='#t1'>table 1</a> above. The first step of the main **DRwithBattery** scenario is the **NoDr** scenario in which the user doesn't care about price at all. This serves as a baseline, and by default the command line output show a comparison of the trial scenario to this baseline.

**Flat Price 1**

  `python3 driver.py scenario.lcl_fixed_price1_scenario -i 2000`

**Flat Price 2**

To generate this result we need to run the main DRwithBattery scenario then use that result to generate the price for this simulation. Edit the report.py script and uncomment the 2 lines about this..

**LCL with no Battery**

This just removes batteries from each house.

  `python3 driver.py scenario.lcl_no_battery_scenario -i 2000`

<style>
  table { width: 100%; }
  table tr { display:flex; flex-flow: row wrap; justify-content: space-around; }
  td { flex: 1; }
</style>

[Sandholm]: http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.70.8213&rep=rep1&type=pdf
