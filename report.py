''' Simple runnable script to drive the network and connects output writers to network. '''
import sys
import os
import argparse
from lcl.writer import *
from lcl.network import *
from lcl.device import *
from lcl.analytics.templates import *
from templates import *


# Parse args.
parser = argparse.ArgumentParser(description='LCL simulation report generator.')
parser.add_argument('data_dir', type=str, help='dir containing simulation results')
parser.add_argument('-p', dest='plots', action='store_true', default=False, help='Gen some plots too')
args = parser.parse_args()
output_dir = args.data_dir.rstrip('/') + '-report'
if not os.path.isdir(args.data_dir):
  print('Not a directory [%s]' % (args.data_dir,))
  sys.exit()
if not os.path.isdir(output_dir):
  os.makedirs(output_dir)

# Load pickled run dump.
replay = StoreNetworkWriter.load(args.data_dir)
init = replay.frame(0)
final = replay.last()

# Make two matrices of difference between init and final for consumption and utility at consumption.
# Note r can ~easily be got via init.s - final.s but need to do this way for u().
print('###### DEVICE CONSUMPTION & UTILITY TABLES')
z = np.zeros(len(init))
header = []
row_labels = [a.id for a in init.agents[0].agents]
device_u_init = []
device_u_final = []
device_r_init = []
device_r_final = []

for i in range(len(init.agents)):
  header += [init.agents[i].id]
  device_u_init += [[d.u(d.r, z) for d in init.agents[i].agents]]
  device_u_final  += [[d.u(d.r, z) for d in final.agents[i].agents]]
  device_r_init += [[d.r.sum() for d in init.agents[i].agents]]
  device_r_final += [[d.r.sum() for d in final.agents[i].agents]]

device_u_init = np.array(device_u_init)
device_u_final = np.array(device_u_final)
device_u_diff = (device_u_init - device_u_final)
device_r_init = np.array(device_r_init)
device_r_final = np.array(device_r_final)
device_r_diff = (device_r_init - device_r_final)
header.insert(0, ' ')
dat = np.vstack((row_labels, np.vectorize(lambda f: '%.4f' % (f,))(device_u_diff))).transpose()
print(output_dir + '/home-by-device-change-in-consumption.csv')
np.savetxt(
  output_dir + '/home-by-device-change-in-utility.csv',
  dat,
  delimiter='|',
  fmt='%s',
  header='|'.join(header),
  comments=''
)
dat = np.vstack((row_labels, np.vectorize(lambda f: '%.4f' % (f,))(device_r_diff))).transpose()
print(output_dir + '/home-by-device-change-in-consumption.csv')
np.savetxt(
  output_dir + '/home-by-device-change-in-consumption.csv',
  dat,
  delimiter='|',
  fmt='%s',
  header='|'.join(header),
  comments=''
)

print('###### DEVICE CONFIGURATION DUMP')
with open(output_dir + '/device-config-dump.txt', 'w') as f:
  for t in ['aircon', 'ecar', 'washer', 'lighting', 'tv-av', 'battery']:
    devices = final.find_agents('.*'+t)
    for k in sorted(devices):
      print(k, devices[k].device, file=f)

print('###### DEVICE DETAILS')
with open(output_dir + '/device-detail.txt', 'w') as f:
  print('###### DEVICE DETAILS INIT', file=f)
  agents = init.find_agents('\.home\d+$')
  for k in sorted(agents):
    print(agents[k], file=f)
  print('###### DEVICE DETAILS FINAL', file=f)
  agents = final.find_agents('\.home\d+$')
  for k in sorted(agents):
    print(agents[k], file=f)

print('###### SUMMARY STATS')
with open(output_dir + '/report.txt', 'w') as f:
  print(report(replay), file=f)

# A flat price scheme. Don't need to iterate, price is derived from DR solution.
# Network.p is a compeuted value based on current consumption. We need to replace it with the flat price.
'''
p = final.flat_pricify()
final.p_of = lambda r: p
print('###### FLAT PRICE 2:\n' + str(final))
print('###### DEVICE DETAILS')
agents = final.find_agents('\.home\d+$')
for k in sorted(agents):
  print(agents[k])
'''

if args.plots:
  print('###### GENERATING PLOTS to %s' % (output_dir))
  report_plots(replay, output_dir) if args.plots else None
  print('###### AIRCON PLOTS')
  aircons = init.find_agents('.*aircon.*')
  print(aircons)
  plot_temperatures(aircons, output_dir + '/aircons-init.png', title='Temperatures of day for each home - Base Case')
  aircons = final.find_agents('.*aircon.*')
  print(aircons)
  plot_temperatures(aircons, output_dir + '/aircons-final.png', title='Temperatures of day for each home - Optimal')
