import sys
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt


ours = np.array(pd.read_table('table1-ours.tsv', sep='|'))
labels = ours[:,0]
ours = np.array(ours[:,1:], dtype=float)
theirs = np.array(np.array(pd.read_table('table1-theirs.tsv', sep='|'))[:,1:], dtype=float)
w = 0.4
widths = np.arange(5)+0.1

print(list(enumerate(labels)))


def set_ax(ax, widths, w):
  ax.set_xticks(widths + w)
  ax.set_xticklabels(('Base', 'FP1', 'FP2', 'LCL no Battery', 'LCL'))


fig, ax = plt.subplots()
set_ax(ax, widths, w)
plt.bar(widths, ours[0], w, color='red', label='ours')
plt.bar(widths+w, theirs[0], w, label='theirs')
plt.ylabel('Load Factor (KW)')
plt.title(labels[0])
plt.legend(loc=2)
plt.savefig('img/load-factor-comparison.png')
plt.close()

fig, ax = plt.subplots()
set_ax(ax, widths, w)
plt.bar(widths, ours[1], w, color='red', label='ours')
plt.bar(widths+w, theirs[1], w, label='theirs')
plt.ylabel('Peak Demand (KW)')
plt.title(labels[1])
plt.legend()
plt.savefig('img/peak-demand-comparison.png')
plt.close()

fig, ax = plt.subplots()
set_ax(ax, widths, w)
plt.bar(widths, ours[2], w, color='red', label='ours')
plt.bar(widths+w, theirs[2], w, label='theirs')
plt.ylabel('Total Demand (KW)')
plt.title(labels[2])
plt.legend()
plt.ylim(140, np.max((ours[2], theirs[2]))+10)
plt.savefig('img/total-demand-comparison.png')
plt.close()

fig, ax = plt.subplots()
set_ax(ax, widths, w)
plt.bar(widths, ours[3], w, color='red', label='ours')
plt.bar(widths+w, theirs[3], w, label='theirs')
plt.ylabel('Total Cost ($)')
plt.title(labels[3])
plt.legend()
plt.savefig('img/total-cost-comparison.png')
plt.close()

fig, ax = plt.subplots()
set_ax(ax, widths, w)
plt.bar(widths, ours[4], w, color='red', label='ours')
plt.bar(widths+w, theirs[4], w, label='theirs')
plt.ylabel('Total Payment ($)')
plt.title(labels[4])
plt.legend()
plt.savefig('img/total-payment-comparison.png')
plt.close()

fig, ax = plt.subplots()
set_ax(ax, widths, w)
plt.bar(widths, ours[5], w, color='red', label='ours')
plt.bar(widths+w, theirs[5], w, label='theirs')
plt.ylabel('Total Utility ($)')
plt.title(labels[5])
plt.ylim(190, np.max((ours[5], theirs[5]))+10)
plt.legend()
plt.savefig('img/total-utility-comparison.png')
plt.close()

fig, ax = plt.subplots()
set_ax(ax, widths, w)
plt.bar(widths, ours[6], w, color='red', label='ours')
plt.bar(widths+w, theirs[6], w, label='theirs')
plt.ylabel('Total Satisfaction ($)')
plt.title(labels[6])
plt.ylim(50, np.max((ours[6], theirs[6]))+10)
plt.legend()
plt.legend(loc=2)
plt.savefig('img/total-satisfaction-comparison.png')
plt.close()

fig, ax = plt.subplots()
set_ax(ax, widths, w)
plt.bar(widths, ours[7], w, color='red', label='ours')
plt.bar(widths+w, theirs[7], w, label='theirs')
plt.ylabel('Welfare ($)')
plt.title(labels[7])
plt.ylim(140, np.max((ours[7], theirs[7]))+10)
plt.legend(loc=2)
plt.savefig('img/welfare-comparison.png')
plt.close()
