###### PROCESS STATISTICS:
steps		= 103
stepsize	= 0.2
num agents	= 8
p stability avg	= 0.001000
r stability avg	= 0.000030
###### DEMAND RESPONSE RESULTS:
load factor	= 0.7942
peak		= 9.2523
demand (tot/avg)	= 176.3669; 7.3486
cost (tot/avg)	= 16.2899; 0.6787
payment		= 32.7954
price (avg)	= 0.1830
utility (tot/avg)	= 200.7587; 25.0948
satisfaction	= 167.9633
welfare		= 184.4688
steps		= 103
stability	= 0.001000/0.001000
stepsize	= 0.200000
###### BASELINE NO DEMAND RESPONSE:
load factor	= 0.4043
peak		= 21.2318
demand (tot/avg)	= 206.0358; 8.5848
cost (tot/avg)	= 37.7844; 1.5743
payment		= 88.6783
price (avg)	= 0.2670
utility (tot/avg)	= 203.5094; 25.4387
satisfaction	= 114.8311
welfare		= 165.7250
steps		= 0
stability	= 8.584823/0.001000
stepsize	= 0.200000
###### DIFF RESPONSE vs BASELINE:
costs:		0.416   0.353   0.189   -0.083  -0.392  -0.647  -0.301  -0.139  -0.035  0.001   -6.356  -5.402  -4.859  -3.890  -3.050  0.092   0.353   0.353   0.353   0.353   0.353   0.353   0.316   0.176  
prices:		0.099   0.077   0.037   -0.014  -0.061  -0.091  -0.048  -0.024  -0.006  0.000   -0.632  -0.561  -0.519  -0.441  -0.369  0.017   0.071   0.071   0.071   0.071   0.071   0.071   0.063   0.033  
consumption:	4.398   3.113   1.321   -0.468  -1.748  -2.370  -1.390  -0.744  -0.207  0.005   -11.980 -11.101 -10.538 -9.482  -8.428  0.562   2.657   2.657   2.657   2.657   2.657   2.657   2.303   1.144  
utilities:	-0.711  -0.473  -0.279  -0.288  -0.274  -0.325  -0.209  -0.191 
payments:	-8.195  -7.484  -6.917  -7.061  -7.062  -6.417  -6.629  -6.118 
satisfactions:	7.484   7.011   6.638   6.773   6.788   6.091   6.420   5.927  
price (tot/avg)	= -21.4944; -0.8956
cost (tot/avg)	= -2.0145; -0.0839
consumption (tot/avg)	= -29.6689; -1.2362
utility (tot/avg)	= -2.7507; -0.3438
peak		= -11.9796
load factor	= 0.3899
welfare		= 18.7438
satisfaction	= 53.1323
payment		= -55.8830

