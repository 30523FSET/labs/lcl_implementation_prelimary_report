#!/bin/bash

for s in lcl_scenario; do
  #time python3 ./lcl/driver.py -s scenario.${s} -i 2000 -t 1e-3 -l 2e-2 -o ${s}-data-large-steps-0.02
  #time python3 ./lcl/driver.py -s scenario.${s} -i 2000 -t 1e-3 -l 1e-1 -o ${s}-data-large-steps-0.1
  time python3 ./lcl/driver.py -s scenario.${s} -i 2000 -t 1e-3 -l 2e-1 -o ${s}-data-large-steps-0.2
done
