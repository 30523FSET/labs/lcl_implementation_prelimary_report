import numpy as np
import matplotlib as mpl

import matplotlib.pyplot as plt


colors = ['red', 'orange', 'yellow', 'purple', 'fuchsia', 'lime', 'green', 'blue', 'navy', 'black']


def plot_temperatures(aircons, output, title):
  ''' `aircons` is a dict of key to Agent pairs '''
  plt.plot(list(aircons.values())[0].t_external, label='external temp', lw=1.4, color='k')

  for i,k in enumerate(sorted(aircons)):
    plt.axhline(aircons[k].t_optimal+np.random.random()*0.1, color=colors[i%len(colors)])
    plt.axhline(aircons[k].t_min+np.random.random()*0.1, color=colors[i%len(colors)], ls='--', lw=0.8)
    plt.axhline(aircons[k].t_max+np.random.random()*0.1, color=colors[i%len(colors)], ls='--', lw=0.8)
    plt.plot(aircons[k].r2t(aircons[k].r), label=k.lstrip('.'), color=colors[i%len(colors)])
  plt.xlim(0,32)
  plt.ylim(list(aircons.values())[0].t_external.min(), list(aircons.values())[0].t_external.max())
  plt.legend()
  plt.title(title)
  plt.savefig(output)
  plt.close()
