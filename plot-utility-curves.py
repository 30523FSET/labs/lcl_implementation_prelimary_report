''' Simple runnable script to drive the network and connects output writers to network. '''
import sys
import os
import argparse
import numpy as np
from numpy import ones, zeros
from lcl.writer import *
from lcl.network import *
from lcl.device import *


def main():
  # Parse args.
  parser = argparse.ArgumentParser(description='LCL simulation report generator.')
  parser.add_argument('data_dir', type=str, help='dir containing simulation results')
  args = parser.parse_args()
  output_dir = args.data_dir.rstrip('/') + '-report'
  if not os.path.isdir(args.data_dir):
    print('Not a directory [%s]' % (args.data_dir,))
    sys.exit()
  if not os.path.isdir(output_dir):
    os.makedirs(output_dir)

  # Load pickled run dump.
  replay = StoreNetworkWriter.load(args.data_dir)
  network = replay.last()
  home1 = network.agents[0]
  ax = np.linspace(0, 1, 50)

  # Show ex lighting device curve from min to max c/w vline where the solution lies.
  l = get(home1, 'lighting')
  ax = np.linspace(0, 1, 50)

  # Plot utility curves samples. Recall u() is a vector function, and not all u()s are separable.
  # We plot a the scalar value of u() on a sweep from the min to max extrema - lbounds/hbounds.
  ax = np.linspace(0, 1, 50)
  for k in ['Lighting', 'Ecar']: # 'Battery', 'Aircon']:
    a = get(home1, k.lower())
    b = bounds(a)
    axx = b[0] + (b[1] - b[0])*ax
    plt.plot(axx, u(a)(ax))
    #plt.axhline(a.u(a.r, zeros(len(a))))
    plt.xlim(b[0], b[1])
    plt.xlabel('Total Power Consumption (min to max constraint))')
    plt.ylabel('Total Utility ($)')
    plt.title('Power Consumption vs Utility for %s (Cummulative)' % (k,))
    # plt.show()
    plt.savefig('img/%s-utility-curve.png' % (k.lower()))
    plt.clf()

  # Better battery plot.
  battery = get(home1, 'battery')
  plt.plot(2*(ax-0.5), u(battery)(ax))
  plt.axvline(0.0)
  plt.xlabel('Total Power Consumption (Scaled (-1,1) - (min,max))')
  plt.ylabel('Total Utility ($)')
  plt.title('Power Consumption vs Utility for Battery')
  plt.savefig('img/battery-utility-curve.png')
  plt.clf()

  # Better TDevice plot.
  ac = get(home1, 'aircon')
  t = np.vectorize(lambda x, ac=ac: IDevice._u(ac.t_min-1+x*(ac.t_max - ac.t_min + 2), 0, 2, 0.25, 0, ac.t_min, ac.t_optimal))
  plt.plot(ax, t(ax))
  plt.axvline(1/(ac.t_max - ac.t_min + 2), label='min temp')
  plt.axvline(1 - 1/(ac.t_max - ac.t_min + 2), label='max temp')
  plt.xlabel('Temperature (Scaled (0,1) - (min,max))')
  plt.ylabel('Total Utility ($)')
  plt.title('Power Consumption vs Utility for Aircon')
  plt.savefig('img/aircon-utility-curve.png')


def get(agent, key):
  for a in agent.agents:
    if a.id == key:
      return a


def u(a):
  return np.vectorize(lambda x, a=a: a.u(x*(a.hbounds - a.lbounds)+a.lbounds, zeros(len(a))) - a.u(a.lbounds, zeros(len(a))))


def bounds(a):
  _min = max(a.cbounds[0] if a.cbounds is not None else float('-inf'), a.lbounds.sum())
  _max = min(a.cbounds[1] if a.cbounds is not None else float('inf'), a.hbounds.sum())
  return (_min,_max)


main()
